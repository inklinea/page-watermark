#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Page Watermark - Some simple Page Watermarking
#  An Inkscape 1.2+ extension
##############################################################################

import inkex
from inkex import Group, Layer, Transform, Use

import datetime



def get_page_translate(self):
    page_list = self.svg.xpath('//inkscape:page')

    page_x_translate_list = []

    for page in page_list:
        x_translate = page.get('x')
        y_translate = page.get('y')
        page_x_translate_list.append([x_translate, y_translate])

    return page_x_translate_list


def paste_objects(self, object_list, page_translate_list):
    parent = self.svg

    time_stamp = 'watermark_dupes' + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")

    watermark_layer = Layer()

    watermark_layer.set('id', str(time_stamp))

    page_index = 1

    for translate in page_translate_list:

        x_translate = translate[0]
        y_translate = translate[1]

        page_watermark_layer = Layer()
        page_watermark_layer.set('id', f'page_{page_index}_{time_stamp}')

        for item in object_list:
            duplicate_item = item.duplicate()
            page_watermark_layer.append(duplicate_item)

            translate_object = Transform()
            translate_object.add_translate(float(x_translate), float(y_translate))

            duplicate_item.transform = translate_object
            duplicate_item.transform = duplicate_item.transform @ item.composed_transform()

            page_watermark_layer.append(duplicate_item)

        watermark_layer.append(page_watermark_layer)

        page_index += 1

    parent.append(watermark_layer)


def paste_clones(self, object_list, page_translate_list):
    parent = self.svg

    time_stamp = 'watermark_clones' + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")

    watermark_layer = Layer()

    watermark_layer.set('id', str(time_stamp))

    page_index = 1

    # Dump the first entry - we don't want a clone on the first page
    page_translate_list.pop(0)

    for translate in page_translate_list:

        x_translate = translate[0]
        y_translate = translate[1]

        page_watermark_layer = Layer()
        page_watermark_layer.set('id', f'page_{page_index}_{time_stamp}')

        for item in object_list:

            clone_item = Use()
            clone_item.href = item.get_id()

            translate_object = Transform()
            translate_object.add_translate(float(x_translate), float(y_translate))

            clone_item.transform = clone_item.transform @ translate_object

            if item.getparent().TAG == 'g':

                clone_item.transform = clone_item.transform @ item.getparent().composed_transform()

            page_watermark_layer.append(clone_item)

        watermark_layer.append(page_watermark_layer)

        page_index += 1

    parent.append(watermark_layer)


class PageWatermark(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--page_watermark_notebook", type=str, dest="page_watermark_notebook", default=0)

        pars.add_argument("--watermark_type_radio", type=str, dest="watermark_type_radio", default='clone')

    def effect(self):

        # Only Inkscape 1.2+ has pages
        if int((inkex.__version__).split('.')[1]) < 2:
            inkex.errormsg('Pages were introduced in Inkscape 1.2+')
            return

        selection_list = self.svg.selected

        if len(selection_list) < 1:
            inkex.errormsg('Nothing Selected !')
            return

        page_translate_list = get_page_translate(self)

        if len(page_translate_list) < 2:
            inkex.errormsg('Only 1 page found - exiting !')
            return

        if self.options.watermark_type_radio != 'clone':
            paste_objects(self, selection_list, page_translate_list)
        else:
            paste_clones(self, selection_list, page_translate_list)


if __name__ == '__main__':
    PageWatermark().run()
